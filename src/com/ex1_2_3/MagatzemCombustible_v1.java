package com.ex1_2_3;

public class MagatzemCombustible_v1 {

    private char[] posicionsEnMagatzem = {'0','0','0','0','0','0','0','0','0','0'};
    int posicio = -1;

    public int numContenidorsAlMagatzem() {
        int contenidorsTotals = 0;
        for (char c : posicionsEnMagatzem) {
            if (c == '1') {
                c++;
            }
        }
        return contenidorsTotals;
    }

    public synchronized void produirContenidorDeCombustible() {
        for (int i = 0; i < posicionsEnMagatzem.length; i++) {
            if (posicionsEnMagatzem[i] == '0') {
                posicionsEnMagatzem[i] = '1';
                posicio = i;
                return;
            }
        }
    }

    public synchronized void consumirContenidorDeCombustible() {
        for (int i = 0; i < posicionsEnMagatzem.length; i++) {
            if (posicionsEnMagatzem[i] == '1') {
                posicionsEnMagatzem[i] = '0';
                return;
            }
        }
    }

    public char[] getPosicionsEnMagatzem() {
        return posicionsEnMagatzem;
    }
}
