package com.ex1_2_3;

public class Exercici_2_v1 {

    public static void inicialitzarPrograma() throws InterruptedException {
        MagatzemCombustible_v1 magatzemCombustible = new MagatzemCombustible_v1();

        DeptCienciaProductor_v1 deptCienciaProductor = new DeptCienciaProductor_v1(magatzemCombustible);
        DeptEnginyeriaConsumidor_v1 deptEnginyeriaConsumidor = new DeptEnginyeriaConsumidor_v1(magatzemCombustible);

        Thread threadCienciaProductor = new Thread(deptCienciaProductor);
        threadCienciaProductor.setName("Fil del departament de Ciència");
        Thread threadEnginyeriaConsumidor = new Thread(deptEnginyeriaConsumidor);
        threadEnginyeriaConsumidor.setName("Fil del departament d'Enginyeria");

        threadCienciaProductor.start();
        threadEnginyeriaConsumidor.start();

        threadCienciaProductor.join();
        threadEnginyeriaConsumidor.join();
    }

}
