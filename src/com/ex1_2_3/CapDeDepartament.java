package com.ex1_2_3;

public class CapDeDepartament implements Runnable {
    private Magatzem magatzem;
    int movimentRacions;

    public CapDeDepartament(Magatzem magatzem, int movimentRacions) {
        this.magatzem = magatzem;
        this.movimentRacions = movimentRacions;
    }

    @Override
    public void run() {
        String threadName = Thread.currentThread().getName();
        System.out.println("Fil iniciat: " + threadName);
        System.out.println("Número de moviment de racions per " + threadName + ": " + movimentRacions);
        System.out.println("Comprovant número total de racions al magatzem per " + threadName + ": " + magatzem.comprovarQuantitatRacions());
        System.out.println("Començant operació per " + threadName + "...");
        if (movimentRacions >= 0) {
            magatzem.retornarRacions(movimentRacions);
        } else {
            magatzem.agafarRacions(movimentRacions);
        }
        System.out.println("Operació per " + threadName + " acabada.");
        System.out.println("Comprovant número total de racions al magatzem per " + threadName + ": " + magatzem.comprovarQuantitatRacions());
        System.out.println("End " + threadName + ".");
    }

}
