package com.ex1_2_3;

import java.util.Arrays;
import java.util.concurrent.Semaphore;

public class SistemaDeGuidaDeTorpedes {

    private int numDeTorpedesQuePotGuiarSimultaneament;
    private Boolean[] usDelSistemaDeGuia;
    private Semaphore semaphore;

    public SistemaDeGuidaDeTorpedes(int totalTorpedes) {
        this.numDeTorpedesQuePotGuiarSimultaneament = totalTorpedes;
        this.usDelSistemaDeGuia = new Boolean[totalTorpedes];
        this.semaphore = new Semaphore(3);
    }

    public int adquirirSistemaDeGuia(String nomThread) {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return assignarSistemaDeGuia(nomThread);
    }

    public synchronized void alliberarSistemaDeGuia(int numUsDelSistemaDeGuia, String nomThread) {
        semaphore.release();

        usDelSistemaDeGuia[numUsDelSistemaDeGuia] = false;

        imprimirUsDelSistemaDeGuia(nomThread);

        semaphore.release();
    }

    public synchronized int assignarSistemaDeGuia(String nomThread) {
        int slotFound = -1;

        for (int i = 0; i < usDelSistemaDeGuia.length; i++) {
            if (!usDelSistemaDeGuia[i]) {
                usDelSistemaDeGuia[i] = true;
                slotFound = i;
            }
        }

        System.out.println(nomThread + ", utilitzant el número: " + slotFound);

        imprimirUsDelSistemaDeGuia(nomThread);

        return slotFound;
    }

    public void imprimirUsDelSistemaDeGuia(String nomThread) {
        System.out.println(nomThread + " Sistema de guia: " + Arrays.toString(usDelSistemaDeGuia));
    }

}
