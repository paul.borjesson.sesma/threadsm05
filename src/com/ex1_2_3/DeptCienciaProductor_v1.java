package com.ex1_2_3;

import java.util.Arrays;

public class DeptCienciaProductor_v1 extends Thread {

    private MagatzemCombustible_v1 magatzemCombustible;

    public DeptCienciaProductor_v1(MagatzemCombustible_v1 magatzemCombustible) {
        this.magatzemCombustible = magatzemCombustible;
    }

    @Override
    public void run() {
        System.out.println("Departament de Ciència iniciat.");
        for (int i = 1; i <= 20; i++) {
            if (magatzemCombustible.numContenidorsAlMagatzem() < 10) {
                System.out.println(i + ". Número de contenidors al magatzem: " + magatzemCombustible.numContenidorsAlMagatzem()
                + Arrays.toString(magatzemCombustible.getPosicionsEnMagatzem()));
                magatzemCombustible.produirContenidorDeCombustible();
                System.out.println("OPERATION EXECUTED\n" + i + ". Número de contenidors al magatzem: " + magatzemCombustible.numContenidorsAlMagatzem()
                        + Arrays.toString(magatzemCombustible.getPosicionsEnMagatzem()));
            }
        }
        System.out.println("Departament de Ciència finalitzat.");
    }

}
