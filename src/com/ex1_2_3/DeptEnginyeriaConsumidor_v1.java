package com.ex1_2_3;

import java.util.Arrays;

public class DeptEnginyeriaConsumidor_v1 extends Thread {

    private MagatzemCombustible_v1 magatzemCombustible;

    public DeptEnginyeriaConsumidor_v1(MagatzemCombustible_v1 magatzemCombustible) {
        this.magatzemCombustible = magatzemCombustible;
    }

    @Override
    public void run() {
        System.out.println("Departament d'Enginyeria iniciat.");
        for (int i = 1; i <= 13; i++) {
            if (magatzemCombustible.numContenidorsAlMagatzem() < 10) {
                System.out.println(i + ". ENG Número de contenidors al magatzem: " + magatzemCombustible.numContenidorsAlMagatzem()
                        + Arrays.toString(magatzemCombustible.getPosicionsEnMagatzem()));
                magatzemCombustible.consumirContenidorDeCombustible();
                System.out.println("OPERATION EXECUTED ENG\n" + i + ". ENG Número de contenidors al magatzem: " + magatzemCombustible.numContenidorsAlMagatzem()
                        + Arrays.toString(magatzemCombustible.getPosicionsEnMagatzem()));
            }
        }
        System.out.println("Departament d'Enginyeria finalitzat.");
    }

}
