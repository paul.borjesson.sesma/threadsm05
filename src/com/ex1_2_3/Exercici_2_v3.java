package com.ex1_2_3;

public class Exercici_2_v3 {
    public static void inicialitzarPrograma() {
        MagatzemCombustible_v3 magatzemDeCombustible_v3 = new MagatzemCombustible_v3();
        DeptCienciaProductor_v3 deptCienciaProductor_v3 = new DeptCienciaProductor_v3(magatzemDeCombustible_v3);
        DeptEnginyeriaConsumidor_v3 deptEnginyeriaConsumidor_v3 = new DeptEnginyeriaConsumidor_v3(magatzemDeCombustible_v3);


        System.out.println("Exercici_4.inicialitzarPrograma() - INICI");
        System.out.println("Exercici_4.magatzemDeCombustible.posicionsEnMagatzem = " + magatzemDeCombustible_v3.posicionsEnMagatzem);

        Thread deptCienciaProductor = new Thread(deptCienciaProductor_v3);
        Thread deptEnginyeriaConsumidor = new Thread(deptEnginyeriaConsumidor_v3);

        deptCienciaProductor.start();
        deptEnginyeriaConsumidor.start();

        try {
            //El main() continuarà la seva execució 5 segons després d'inicialitzar productorDeLLetresThread i
            //consumidorDeLLetresThread independentment que aquest hagin acabat la seva execució.
            deptCienciaProductor.join(5000);
            deptEnginyeriaConsumidor.join(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Exercici_4.magatzemDeCombustible.posicionsEnMagatzem = " + magatzemDeCombustible_v3.posicionsEnMagatzem);
        System.out.println("Exercici_4.inicialitzarPrograma() - FI");
    }
}
