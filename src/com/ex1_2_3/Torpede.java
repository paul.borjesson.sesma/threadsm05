package com.ex1_2_3;

public class Torpede implements Runnable{

    private SistemaDeGuidaDeTorpedes sistemaDeGuidaDeTorpedes;

    public Torpede(SistemaDeGuidaDeTorpedes sistemaDeGuidaDeTorpedes) {
        this.sistemaDeGuidaDeTorpedes = sistemaDeGuidaDeTorpedes;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " running.");

        int position = sistemaDeGuidaDeTorpedes.adquirirSistemaDeGuia(Thread.currentThread().getName());

        try {
            Thread.currentThread().sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        sistemaDeGuidaDeTorpedes.alliberarSistemaDeGuia(position, Thread.currentThread().getName());

        System.out.println(Thread.currentThread().getName() + " finished.");
    }
}
