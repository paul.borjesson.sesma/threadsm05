package com.ex1_2_3;

public class Exercici_2_v6 {

    public static void inicialitzarPrograma() throws InterruptedException {
        SistemaDeGuidaDeTorpedes sistemaDeGuiaDeTorpedes = new SistemaDeGuidaDeTorpedes(3);

        Thread[] torpedes = new Thread[10];

        for (int i = 0; i < torpedes.length; i++) {
            Torpede torpede = new Torpede(sistemaDeGuiaDeTorpedes);

            torpedes[i] = new Thread(torpede);

            torpedes[i].setName("Torpede " + i);
            torpedes[i].start();
            torpedes[i].join();
        }

        sistemaDeGuiaDeTorpedes.imprimirUsDelSistemaDeGuia("FINAL");

    }

}
