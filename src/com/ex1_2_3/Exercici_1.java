package com.ex1_2_3;

public class Exercici_1 {

    static long filID;

    public static void inicialitzarPrograma() throws InterruptedException {
        //Creem un magatzem comú, tothom visita la mateixa entitat de magatzem
        Magatzem magatzem = new Magatzem();

        //Creem els caps de departament
        CapDeDepartament capComandament = new CapDeDepartament(magatzem, 100);
        CapDeDepartament capArmes = new CapDeDepartament(magatzem, 100);
        CapDeDepartament capTiN = new CapDeDepartament(magatzem, -30);
        CapDeDepartament capEnginyeria = new CapDeDepartament(magatzem, 1000);
        CapDeDepartament capCiència = new CapDeDepartament(magatzem, -50);

        //Creem els fils per a aquests departaments
        Thread filCapComandament = new Thread(capComandament);
        filCapComandament.setName("Fil del departament \"Comandament\"");
        filID = filCapComandament.getId();
        Thread filCapArmes = new Thread(capArmes);
        filCapArmes.setName("Fil del departament \"Armes\"");
        Thread filCapTiN = new Thread(capTiN);
        filCapTiN.setName("Fil del departament \"Timó i navegació\"");
        Thread filCapEnginyeria = new Thread(capEnginyeria);
        filCapEnginyeria.setName("Fil del departament \"Enginyeria\"");
        Thread filCapCiència = new Thread(capCiència);
        filCapCiència.setName("Fil del departament \"Ciència\"");

        //Inicialitzem els fils
        filCapComandament.start();
        filCapArmes.start();
        filCapTiN.start();
        filCapEnginyeria.start();
        filCapCiència.start();

        //Demanem a aquesta funció que s'esperi als fils per acabar abans d'acabar ella.
        filCapComandament.join();
        filCapArmes.join();
        filCapTiN.join();
        filCapEnginyeria.join();
        filCapCiència.join();

    }
}
