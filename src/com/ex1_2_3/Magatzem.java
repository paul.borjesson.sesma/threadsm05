package com.ex1_2_3;

public class Magatzem {
    private int quantitatRacions = 1000;

    public synchronized void retornarRacions(int numRacions) {
        if (Thread.currentThread().getId() == Exercici_1.filID) {
            try {
                Thread.sleep(5000);
                System.out.println("Thread " + Thread.currentThread().getName() + " sleeping for 5 seconds...");
                //TODOS LOS MÉTODOS SYNCHRONIZED DE UN OBJETO SE LOCKEAN, AQUÍ LA PRUEBA.
                //Te muestra el print del objeto departamento (el que espera 5000ms) antes que otros, usen este método o el de agafar racions
                //Consiguientemente, comandament lockea el objeto entero, no solo este método :)
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        quantitatRacions += numRacions;
    }

    public synchronized void agafarRacions(int numRacions) {
        quantitatRacions -= numRacions;
    }

    public synchronized int comprovarQuantitatRacions() {
        return quantitatRacions;
    }
}
