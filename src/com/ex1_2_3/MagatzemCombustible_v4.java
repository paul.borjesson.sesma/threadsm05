package com.ex1_2_3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Semaphore;

public class MagatzemCombustible_v4 {
    public ArrayList<Character> posicionsEnMagatzem = new ArrayList<Character>();
    private Semaphore semaforPosicions;

    public MagatzemCombustible_v4() {
        Character[] zeros = {'0','0','0','0','0','0','0','0','0','0'};
        posicionsEnMagatzem.addAll(Arrays.asList(zeros));

        semaforPosicions = new Semaphore(1);
    }

    public int numContenidorsAlMagatzem() {
        int totalContenidors = 0;
        for (char c : posicionsEnMagatzem) {
            if (c == '1') {
                totalContenidors++;
            }
        }
        return totalContenidors;
    }

    public boolean produirContenidorDeCombustible() {
        int position;
        boolean success = false;
        if (numContenidorsAlMagatzem() < 10) {
            semaforPosicions.tryAcquire();
            System.out.println("1111 - numContenidorsAlMagatzem() = " + numContenidorsAlMagatzem() + ", " + posicionsEnMagatzem);
            position = posicionsEnMagatzem.indexOf('0');
            posicionsEnMagatzem.set(position, '1');

            success = true;
        }

        System.out.println("1111 - SUCCESS?" + success + ",  + numContenidorsAlMagatzem() = " + numContenidorsAlMagatzem() + ", " + posicionsEnMagatzem);

        semaforPosicions.release();

        return success;
    }

    public boolean consumirContenidorDeCombustible() {
        int position;
        boolean success = false;
        if (numContenidorsAlMagatzem() > 0) {
            semaforPosicions.tryAcquire();
            System.out.println("2222 - numContenidorsAlMagatzem() = " + numContenidorsAlMagatzem() + ", " + posicionsEnMagatzem);
            position = posicionsEnMagatzem.indexOf('1');
            posicionsEnMagatzem.set(position, '0');

            success = true;
        }

        System.out.println("2222 - SUCCESS?" + success + ",  + numContenidorsAlMagatzem() = " + numContenidorsAlMagatzem() + ", " + posicionsEnMagatzem);

        semaforPosicions.release();

        return success;
    }

}
