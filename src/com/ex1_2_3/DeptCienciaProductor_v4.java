package com.ex1_2_3;

public class DeptCienciaProductor_v4 implements Runnable {
    MagatzemCombustible_v4 magatzemCombustible = new MagatzemCombustible_v4();

    public DeptCienciaProductor_v4(MagatzemCombustible_v4 magatzemCombustible) {
        this.magatzemCombustible = magatzemCombustible;
    }

    @Override
    public void run() {
        int i;
        boolean exitOperacio;


        System.out.println("1111 - DeptCienciaProductor.INICI");

        i = 0;
        while (i < 20) {
            exitOperacio = magatzemCombustible.produirContenidorDeCombustible();

            if (exitOperacio == true) {
                i++;
            }
        }

        System.out.println("1111 - DeptCienciaProductor.FI");
    }
}
